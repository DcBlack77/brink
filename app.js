var	express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var multer = require('multer');
var cloudinary = require('cloudinary');
var pass_app = "123";
var method_override= require("method-override");


cloudinary.config({
	cloud_name: "dcblack77",
	api_key: "229868825344196",
	api_secret:"AJh_v0gWZ8z0gamAKzSdl_izWyQ"

});

var app = express ();

mongoose.connect("mongodb://localhost/listaSO");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
var uploader = multer({dest: "./uploads"});
var middleware_upload = uploader.single('imagen');
app.use(method_override("_method"));

//Definir schema de los productos
var productschema = {
	id: String,
	distribuidora: String,
	categoria: String,
	nombre: String,
	descripcion: String,
	imagen: String,
	precio: Number
};

var Product = mongoose.model("Product", productschema);


app.set ("view engine", "jade");
app.use(express.static ("staticpublic"));
app.get("/",function (req,res) {
	res.render("index");
});

app.get("/error",function(req,res) {
	res.render("error/index");
});

app.put("/menu/:id",middleware_upload,function(req,res){
	if(req.body.password == pass_app ){
		var data = {
				id: req.body.id,
				distribuidora: req.body.distribuidora,
				categoria: req.body.categoria,
				nombre: req.body.nombre,
				descripcion: req.body.descripcion,
				precio: req.body.precio
			};

		Product.update({"id":req.params.id},data,function(product){
			res.redirect("/menu");
			console.log(data);
		});



	}else{
		res.redirect("/");
	}

});

app.get("/menu/edit/:id",function(req,res){
	var id_p = req.params.id;
	console.log(id_p);
	Product.findOne({"id":id_p},function(err,prod){
		console.log(prod);
		res.render("menu/edit", { product: prod});
	});

});

app.get("/menu", function(req,res){
	Product.find( function(err,doct){
		if(err){ console.log(err); }
		res.render("menu/index", {products: doct  })
	});
});

app.get("/menu/brink_tools", function(req,res){
	Product.find( {distribuidora:"Brink Tools"}, function(err,distri){
		if(err){console.log(err);}
		res.render("menu/brink_tools", {products: distri})
	});
})

app.get("/menu/brink_tools/hmanu", function(req,res){
	Product.find( {distribuidora:"Brink Tools", categoria:"Herramientas Manuales"}, function(err,categoria){
		if(err){ console.log(err); }
		res.render("menu/brink_tools/hmanu", {products: categoria})
	});
});

app.get("/menu/brink_tools/melec", function(req,res){
	Product.find( {categoria:"Materiales Eléctricos"}, function(err,categoria){
		if(err){ console.log(err); }
		res.render("menu/brink_tools/melec", {products: categoria})
	});
});

app.get("/menu/brink_tools/hogar", function(req,res){
	Product.find( {categoria:"Hogar"}, function(err,categoria){
		if(err){ console.log(err); }
		res.render("menu/brink_tools/hogar", {products: categoria})
	});
});

app.get("/menu/proqui", function(req,res){
	Product.find( {categoria:"Productos Químicos"}, function(err,categoria){
		if(err){ console.log(err); }
		res.render("menu/proqui", {products: categoria})
	});
});

app.get("/menu/grifplo", function(req,res){
	Product.find( {categoria:"Grifería y Plomería"}, function(err,categoria){
		if(err){ console.log(err); }
		res.render("menu/grifplo", {products: categoria})
	});
});

app.get("/menu/cerra", function(req,res){
	Product.find( {categoria:"Cerrajería"}, function(err,categoria){
		if(err){ console.log(err); }
		res.render("menu/cerra", {products: categoria})
	});
});

app.get("/menu/herr", function(req,res){
	Product.find( {categoria:"Herreria"}, function(err,categoria){
		if(err){ console.log(err); }
		res.render("menu/herr", {products: categoria})
	});
});

app.get("/menu/alba", function(req,res){
	Product.find( {categoria:"Albañilería"}, function(err,categoria){
		if(err){ console.log(err); }
		res.render("menu/alba", {products: categoria})
	});
});

app.get("/menu/abra", function(req,res){
	Product.find( {categoria:"Abrasivos"}, function(err,categoria){
		if(err){ console.log(err); }
		res.render("menu/abra", {products: categoria})
	});
});

app.get("/menu/seg", function(req,res){
	Product.find( {categoria:"Seguridad"}, function(err,categoria){
		if(err){ console.log(err); }
		res.render("menu/seg", {products: categoria})
	});
});

app.get("/menu/aut", function(req,res){
	Product.find( {categoria:"Automotriz"}, function(err,categoria){
		if(err){ console.log(err); }
		res.render("menu/aut", {products: categoria})
	});
});

app.get("/menu/jar", function(req,res){
	Product.find( {categoria:"Jardinería"}, function(err,categoria){
		if(err){ console.log(err); }
		res.render("menu/jar", {products: categoria})
	});
});

app.get("/menu/misc", function(req,res){
	Product.find( {categoria:"Misceláneos"}, function(err,categoria){
		if(err){ console.log(err); }
		res.render("menu/misc", {products: categoria})
	});
});

app.get("/menu/helec", function(req,res){
	Product.find( {categoria:"Herramientas Eléctricas"}, function(err,categoria){
		if(err){ console.log(err); }
		res.render("menu/helec", {products: categoria})
	});
});

app.get("/menu/com", function(req,res){
	Product.find( {categoria:"Equipos a Combustión"}, function(err,categoria){
		if(err){ console.log(err); }
		res.render("menu/com", {products: categoria})
	});
});

app.get("/menu/ilu", function(req,res){
	Product.find( {categoria:"Iluminación"}, function(err,categoria){
		if(err){ console.log(err); }
		res.render("menu/ilu", {products: categoria})
	});
});


app.post("/admin",function(req,res){
	if(req.body.password == pass_app ){
		Product.find(function(err,doct){
			if(err){ console.log(err); }
			res.render("admin/index", {products: doct  })
	});
	}else{
		res.redirect("/");
	}
});

app.get("/admin",function(req,res){
	res.render("admin/form");
});

app.post("/menu",middleware_upload,function(req,res){

	if(req.body.password == pass_app ){
		var data = {
				id: req.body.id,
				categoria: req.body.distribuidora,
				categoria: req.body.categoria,
				nombre: req.body.nombre,
				descripcion: req.body.descripcion,
				imagen:"data.jpg",
				precio: req.body.precio,

			}

			var product = new Product(data);

			if(req.file){
			req.file.imagen;
				cloudinary.uploader.upload(req.file.path, function(result) {
				  product.imagen = result.url
				  product.save(function(err){
					console.log(product);
					res.render("index");
				});
				});

			/*product.save(function(err){
				console.log(product);
				res.render("index");
			});*/
	};
	}else {
		res.render("menu/new");
	}
});

app.get("/menu/new",function(req,res) {
	res.render("menu/new")
});

app.get("/menu/delete/:id",function(req,res){
	var id = req.params.id;
	Product.findOne({"id":id},function(err,prod){
		res.render("menu/delete", { prod: prod});
	});
});

app.delete("/menu/:id",function(req,res){
	var id = req.params.id;

	if(req.body.password == pass_app ){
		//console.log(req.body.password);
		Product.remove({"id" : id },function(err){
			if(err){console.log(err);}
			res.redirect("/menu");
		});
	}else{
		res.redirect("../error");
	}
});

app.listen(8053);
console.log("Servidor corriendo en el puerto 8053");
